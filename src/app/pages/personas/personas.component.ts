import { Component, OnInit } from '@angular/core';
import { PersonasService } from 'src/app/services/personas.service';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {
  
  listaUsuarios = [];

  constructor( public _tipoReferenciaService: PersonasService) {
   
   }



  ngOnInit(): void {
    this.cargarUsuarios();
      }

  cargarUsuarios() {
    this._tipoReferenciaService.getUsuarios( )
              .subscribe(
                (resp: any) => {
                 this.listaUsuarios = resp;
                 console.log(resp.Mensaje);
              });

  }

  adicionarUsuario(item:any): void {
    this._tipoReferenciaService.crearUsuario(item.data)
    .subscribe(
      resp => {
        this.cargarUsuarios();
        }
    );
}

actualizarUsuario(item:any): void {
  this._tipoReferenciaService.actualizarUsuario(item.data)
  .subscribe(
    resp => {
      this.cargarUsuarios();
      }
  );
}

eliminarUsuarios(item:any): void {
    this._tipoReferenciaService.deleteUsuario(item.key)
  .subscribe(
    resp => {
      this.cargarUsuarios();
      }
  );
  }

  

}









