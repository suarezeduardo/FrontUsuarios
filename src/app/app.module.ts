import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { HeaderComponent } from './componentes/header/header.component';
import { BodyComponent } from './componentes/body/body.component';
import { NavbarComponent } from './componentes/shared/navbar/navbar.component';
import { HomeComponent } from './componentes/home/home.component';

// Rutas
import { APP_ROUTING } from './app.routes';
import { ParametrizacionComponent } from './pages/parametrizacion/parametrizacion.component';
import { PersonasComponent } from './pages/personas/personas.component';
import { EntidadesComponent } from './pages/entidades/entidades.component';
import { ServiceModule } from './services/service.module';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DxBulletModule, DxDataGridModule, DxTemplateModule } from 'devextreme-angular';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    BodyComponent,
    NavbarComponent,
    HomeComponent,
    ParametrizacionComponent,
    PersonasComponent,
    EntidadesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    APP_ROUTING,
    ServiceModule,
    FormsModule,
    NgSelectModule,
    DxDataGridModule,
    DxTemplateModule,
    DxBulletModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
