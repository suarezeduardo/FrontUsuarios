import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {


  ano = 0;
  mes= 0;
  dia= 0;

  
  fecha: string ;

  constructor() { 
    this.ano =  new Date().getFullYear();
    this.mes =  new Date().getMonth();
    this.dia =  new Date().getDay();
    this.fecha = this.dia.toString() + '-' + this.mes.toString()  + '-' + this.ano.toString(); 
  }

  ngOnInit(): void {

  }

}
