import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  ocultarGrid: boolean;

  constructor() {
    this.ocultarGrid = true;
   }

  ngOnInit(): void {
  }

  mostrar(){

    this.ocultarGrid = false;
  }

  mostrarNuevo(){

    this.ocultarGrid = true;
  }

}
