
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './componentes/home/home.component';
import { EntidadesComponent } from './pages/entidades/entidades.component';
import { ParametrizacionComponent } from './pages/parametrizacion/parametrizacion.component';
import { PersonasComponent } from './pages/personas/personas.component';


const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  //  { path: 'parametrizacion', component: ParametrizacionComponent },
   { path: 'personas', component: PersonasComponent },
  //  { path: 'entidades', component: EntidadesComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'personas' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});