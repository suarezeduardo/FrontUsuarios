import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonasService} from './service.index';







@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [PersonasService],
  declarations: []
})
export class ServiceModule { }