import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import Swal from 'sweetalert2';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {
  api: string;
  constructor(public http: HttpClient) {
    this.api = environment.apiUrl;
   }

 

  getUsuarios(){
    const url = this.api;
    return this.http.get(url + 'usuarios/');
  }

  crearUsuario( perfil: Usuarios ) {
    const json = JSON.stringify(perfil);
        const params = json;
        const headers = new HttpHeaders().set('Content-Type', 'application/json', );
        const url = this.api + 'usuarios';
     return this.http.post(url, params, { headers: headers})
     .pipe(
     map( (resp: any) => {
      //Swal.fire('Usuarios Creado', resp.Mensaje, 'success' );
      return resp.usuario;
     }));
  }

  deleteUsuario(id:any){
    const headers = new HttpHeaders().set('Content-Type', 'text/plain');
    debugger;
    return this.http.delete(this.api  + 'usuarios?id=' + id,{ headers: headers});
    // .pipe(
    //   map( (resp: any) => {
    //     //Swal.fire('Usuario Eliminado Exitosamente', resp.Mensaje, 'success' );
    //    return resp.usuario;
    //   }));
}


  actualizarUsuario( perfil: Usuarios ) {
    const json = JSON.stringify(perfil);
        const params = json;
        const headers = new HttpHeaders().set('Content-Type', 'application/json', );
        const url = this.api + 'usuariosUpdate';
     return this.http.post(url, params, { headers: headers})
     .pipe(
     map( (resp: any) => {
      //Swal.fire('Usuario Actualizado Exitosamente', resp.Mensaje, 'success' );
      return resp.usuario;
     }));
  }

}


export class Usuarios {
  constructor(
        public id: string,
        public createAt: string,
        public segundoNombre: string,
        public edad: string,
        public sexo: string,
        public email: string,
        public numeroCelular: string,
        public primerNombre: string,
        public primerApellido: string,
        public numeroIdentificacion: string,
        public segundoApellido: string,
  ) { }
}
